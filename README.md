# Containers Cleaner

The script which clean docker containers by image or version tag of the image.

**Very useful to use on `staging` and `production` environment.**

**Work with docker containers which have version tag one of that: `0.0`, `0.0.0`, `0.0.0.0`, `v0.0`, `v0.0.0`, `v0.0.0.0`**



_Creator_: Oleksii Pavliuk pavliuk.aleksey@gmail.com

_GitLab_: @opavliuk

_Version_: `1.0.0`

## Usage
```sh
Usage: ./containers_cleaner [image] [versions count stay alive]
```

### Crontab
Set on your servers: `5 * * * * {path}/containers_cleaner {image name} {versions count stay alive}`

## Example:
```sh
> ./containers_cleaner nginx 2
Clean containers of old versions image: nginx
Stay alive: 2 old version
current version: 1.0.13
stay alive version: 1.0.12
stay alive version: 1.0.11
delete old version: 1.0.10
```

